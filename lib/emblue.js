const soap 		= require('soap');
const url 		= 'http://api.embluemail.com/Services/Emblue3Service.svc/json/';
const request = require('request');

class Emblue {
	constructor(props) {
		this.user = props.user;
		this.pass = props.pass;
		this.token = props.token;

		//this.dToken = 'gZ19FX9E-Mji3d-wJovz-Vjdtkl78yc';

		this.authenticate = this.authenticate.bind(this);
		this.action = this.action.bind(this);
	}

	authenticate() {
		return new Promise((resolve, reject) => {
			this.action('Authenticate', {
				User: this.user,
				Pass: this.pass,
				Token: this.token
			}).then((res) => {
				if(!res.Token) throw new Error('Auth error');

				this.dToken = res.Token;

				return resolve();
			});
		});
	}

	action(method, params) {
		return new Promise((resolve, reject) => {

			let fetch = () => {
				if(this.dToken) {
					params.Token = this.dToken;
					console.log(params.Token);
				}

				request({
					json: params,
					method: 'POST',
					url: url + method,
					headers: {
						'Content-Type': 'application/json'
					},
	    		gzip: true
				}, (err, res, body) => {

					if(body.Code == 1) {
						params.Token = null;
						this.dToken = null;
						return resolve(this.action(method, params));
					}

					return resolve(body);
				})
			}

			if(params.Token || this.dToken) {
				fetch()
			} else {
				this.authenticate().then(fetch)
			}
		});
	}
}

module.exports = Emblue;

